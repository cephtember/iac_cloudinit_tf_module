output "master_bind9_node" {
  value = proxmox_virtual_environment_vm.master_bind9_node
}

output "master_loadbalancer_node" {
  value = proxmox_virtual_environment_vm.master_loadbalancer_node
}

output "master_control_plane_node" {
  value = proxmox_virtual_environment_vm.master_control_plane_node
}

output "additional_control_plane_nodes" {
  value = proxmox_virtual_environment_vm.additional_control_plane_nodes
}

output "master_worker_node" {
  value = proxmox_virtual_environment_vm.master_worker_node
}

output "additional_worker_nodes" {
  value = proxmox_virtual_environment_vm.additional_worker_nodes
}