locals {
  dns_reversed_ip     = join(".", reverse(split(".", var.dns_ipaddress)))
  dns_ip_parts        = split(".", local.dns_reversed_ip)
  dns_arpa_part_value = "${local.dns_ip_parts[2]}.${local.dns_ip_parts[3]}"
  dns_last_octet      = local.dns_ip_parts[0]
  dns_arpa            = "0.${local.dns_arpa_part_value}.in-addr.arpa"

  master_ip_ha_parts   = split(".", var.k8s_master_ipaddress_ha)
  master_ha_last_octet = local.master_ip_ha_parts[3]

  kubernetes_version       = split(".", var.kubernetes_version)
  kubernetes_version_minor = "${local.kubernetes_version[0]}.${local.kubernetes_version[1]}"
}


resource "local_file" "master_bind9_node_snippet" {
  filename        = var.master_bind9_node_snippet.path
  file_permission = "666"
  content         = <<-EOT
#cloud-config
hostname: bind9-master-0
fqdn: bind9-master-0
manage_etc_hosts: true
package_upgrade: true
packages:
  - qemu-guest-agent
  - htop
  - git
  - mc
  - ca-certificates
  - curl
  - gnupg-agent
  - software-properties-common
  - open-iscsi
  - nfs-common
  - ansible
  - gnupg2 
  - apt-transport-https 
  - lsb-release
  - python3-pip
  - bind9

timezone: ${var.timezone}

users:
  - name: ${var.cloud_init_user.name}
    gecos: ${var.cloud_init_user.gecos}
    sudo: 'ALL=(ALL) NOPASSWD:ALL'
    groups: [users, admin, docker]
    passwd: ${var.cloud_init_user.password}
    shell: /bin/bash
    lock-passwd: false
    ssh_pwauth: true
    ssh_authorized_keys:
        %{~for keys in var.cloud_init_user.ssh_keys~}
        - ${keys};
        %{~endfor~}
ca_certs:
  remove_defaults: false
  trusted:
  - |
${var.trusted_certs_bundle}

write_files:
  - path: /etc/bind/rndc.key
    content: |
      key "rndc-key" {
        algorithm ${var.dns_rndc_key.tsigAlgorithm};
        secret "${var.dns_rndc_key.tsigSecret}";
      };

  - path: /etc/bind/named.conf.options
    content: |
      acl goodclients {
        %{~for ip in var.dns_acl_good_client~}
        ${ip};
        %{~endfor~}
        localhost;
        localnets;
      };

      options {
        directory "/var/cache/bind";

        recursion yes;

        allow-query { goodclients; };
        
        forwarders {
          %{~for ip in var.dns_forwarders~}
          ${ip};
          %{~endfor~}
        };

        //========================================================================
        // If BIND logs error messages about the root key being expired,
        // you will need to update your keys.  See https://www.isc.org/bind-keys
        //========================================================================
        dnssec-validation auto;

        listen-on-v6 { any; };
      };

  - path: /etc/bind/named.conf.local
    content: |
      include "/etc/bind/rndc.key";
      zone "${var.dns_zone}" {
        type master;
        file "/var/cache/bind/forward.${var.dns_zone}";
        allow-transfer { key rndc-key; };
        allow-update { key rndc-key; };
      };

      zone "${local.dns_arpa}" {
        type master;
        file "/var/cache/bind/reverse.${var.dns_zone}";
        allow-transfer { key rndc-key; };
        allow-update { key rndc-key; };
      };

  - path: /var/cache/bind/reverse.${var.dns_zone}
    content: |
      $TTL    604800
      @       IN      SOA     ns1.${var.dns_zone}. root.ns1.${var.dns_zone}. (
                                    1
                              604800
                                86400
                              2419200
                              604800 )
      @       IN      NS      ns1.${var.dns_zone}.
      ns1     IN      A       ${var.dns_ipaddress}
      ${local.dns_last_octet}      IN      PTR     ns1.${var.dns_zone}.
      ${local.master_ha_last_octet}      IN      PTR     master.${var.dns_zone}.
      
  - path: /var/cache/bind/forward.${var.dns_zone}
    content: |
      $TTL    604800
      @       IN      SOA     ns1.${var.dns_zone}. root.ns1.${var.dns_zone}. (
                                    2         ; Serial
                              604800         ; Refresh
                                86400         ; Retry
                              2419200         ; Expire
                              604800 )       ; Negative Cache TTL
      @       IN      NS      ns1.${var.dns_zone}.
      ns1     IN      A       ${var.dns_ipaddress}
      master  IN      A       ${var.k8s_master_ipaddress_ha}
      @       IN      AAAA    ::1

runcmd:
  - |
    apt update
    apt install -y qemu-guest-agent
    systemctl enable qemu-guest-agent 
    systemctl start qemu-guest-agent
    systemctl restart bind9

power_state:
  delay: now
  mode: reboot
  message: Rebooting after cloud-init completion
  condition: true   
  EOT
}


resource "local_file" "master_loadbalancer_node_snippet" {
  filename        = var.master_loadbalancer_node_snippet.path
  file_permission = "666"
  content         = <<-EOT
#cloud-config
hostname: k8s-controller-lb
fqdn: k8s-controller-lb
manage_etc_hosts: true
package_upgrade: true
packages:
  - qemu-guest-agent
  - htop
  - git
  - mc
  - ca-certificates
  - curl
  - gnupg-agent
  - software-properties-common
  - haproxy

timezone: ${var.timezone}

users:
  - name: ${var.cloud_init_user.name}
    gecos: ${var.cloud_init_user.gecos}
    sudo: 'ALL=(ALL) NOPASSWD:ALL'
    groups: [users, admin, docker]
    passwd: ${var.cloud_init_user.password}
    shell: /bin/bash
    lock-passwd: false
    ssh_pwauth: true
    ssh_authorized_keys:
        %{~for keys in var.cloud_init_user.ssh_keys~}
        - ${keys};
        %{~endfor~}

ca_certs:
  remove_defaults: false
  trusted:
  - |
${var.trusted_certs_bundle}

runcmd: 
  - |
    mkdir -p /etc/systemd/resolved.conf.d/
    echo "[Resolve]" > /etc/systemd/resolved.conf.d/dns_servers.conf
    echo "DNS=${var.master_bind9_node.bind9_master.network.ipv4}" >> /etc/systemd/resolved.conf.d/dns_servers.conf
    systemctl restart systemd-resolved
    apt install -y qemu-guest-agent
    systemctl enable qemu-guest-agent 
    systemctl start qemu-guest-agent
    cat <<EOF >> /etc/haproxy/haproxy.cfg
    frontend kubernetes
        bind ${var.k8s_master_ipaddress_ha}:6443
        option tcplog
        mode tcp
        default_backend kubernetes-master-nodes

    backend kubernetes-master-nodes
        mode tcp
        balance roundrobin
        option tcp-check
        server k8s-controller-000000 ${var.kubernetes_settings.master_ipaddress_0}:6443 check fall 3 rise 2
        server k8s-controller-1 ${var.kubernetes_settings.master_ipaddress_1}:6443 check fall 3 rise 2
        server k8s-controller-2 ${var.kubernetes_settings.master_ipaddress_2}:6443 check fall 3 rise 2
    EOF
    systemctl restart haproxy
    
power_state:
    delay: now
    mode: reboot
    message: Rebooting after cloud-init completion
    condition: true
EOT
}


resource "local_file" "master_control_plane_node_snippet" {
  filename        = var.master_control_plane_node_snippet.path
  file_permission = "666"
  content         = <<-EOT
#cloud-config
hostname: k8s-controller-000000
fqdn: k8s-controller-000000
manage_etc_hosts: true
package_upgrade: true
packages:
  - qemu-guest-agent
  - htop
  - git
  - mc
  - ca-certificates
  - curl
  - gnupg-agent
  - software-properties-common
  - open-iscsi
  - nfs-common
  - ansible
  - gnupg2 
  - apt-transport-https 
  - lsb-release
  - python3-pip
  - blkid
  - findmnt

timezone: ${var.timezone}
users:
  - name: ${var.cloud_init_user.name}
    gecos: ${var.cloud_init_user.gecos}
    sudo: 'ALL=(ALL) NOPASSWD:ALL'
    groups: [users, admin, docker]
    passwd: ${var.cloud_init_user.password}
    shell: /bin/bash
    lock-passwd: false
    ssh_pwauth: true
    ssh_authorized_keys:
        %{~for keys in var.cloud_init_user.ssh_keys~}
        - ${keys};
        %{~endfor~}
ca_certs:
  remove_defaults: false
  trusted:
  - |
${var.trusted_certs_bundle}

runcmd:
  - |
    mkdir -p /etc/systemd/resolved.conf.d/
    echo "[Resolve]" > /etc/systemd/resolved.conf.d/dns_servers.conf
    echo "DNS=${var.master_bind9_node.bind9_master.network.ipv4}" >> /etc/systemd/resolved.conf.d/dns_servers.conf
    systemctl restart systemd-resolved
    mkdir -p /etc/apt/keyrings
    [ ! -f /etc/apt/keyrings/docker.gpg ] && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    [ ! -f /etc/apt/sources.list.d/docker.list ] && echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

    curl -fsSL https://pkgs.k8s.io/core:/stable:/v${local.kubernetes_version_minor}/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
    echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v${local.kubernetes_version_minor}/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
    ln -s /usr/bin/kubectl /usr/local/bin/k
    apt update
    apt install -y qemu-guest-agent
    systemctl enable qemu-guest-agent 
    systemctl start qemu-guest-agent
    apt install -y containerd.io 
    containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1
    sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
    systemctl restart containerd
    systemctl enable containerd
    apt install -y kubelet=${var.kubernetes_version} kubeadm=${var.kubernetes_version} kubectl=${var.kubernetes_version}
    apt-mark hold kubelet kubeadm kubectl
    modprobe br_netfilter
    modprobe overlay
    sleep 15
    echo 1 > /proc/sys/net/bridge/bridge-nf-call-iptables
    echo 1 > /proc/sys/net/ipv4/ip_forward
    kubeadm init --token-ttl=0 --token=${var.kubernetes_settings.token} \
    --certificate-key=${var.kubernetes_settings.certificate_key} \
    --control-plane-endpoint "master.${var.dns_zone}:6443" --upload-certs \
    --pod-network-cidr=${var.kubernetes_settings.pod_network_cidr} --service-cidr=${var.kubernetes_settings.service_network_cidr}
    KUBECONFIG=/etc/kubernetes/admin.conf kubectl apply -f https://raw.githubusercontent.com/projectcalico/calico/v3.26.4/manifests/calico.yaml
    mkdir -p /home/${var.cloud_init_user.name}/.kube
    cp /etc/kubernetes/admin.conf /home/${var.cloud_init_user.name}/.kube/config
    chown -R ${var.cloud_init_user.name}:${var.cloud_init_user.name} /home/${var.cloud_init_user.name}/.kube
 
#cloud-config
write_files:
  - path: /etc/cron.d/ddns_update
    content: |
      */5 * * * * root bash /opt/ddns-update

  - path: /opt/rndc.key
    content: |
     key "rndc-key" {
      algorithm ${var.dns_rndc_key.tsigAlgorithm};
      secret "${var.dns_rndc_key.tsigSecret}";
     };

  - path: /opt/ddns-update
    permissions: '0755'
    content: |
      KEYFILE=/opt/rndc.key
      NAMESERVER="${var.dns_ipaddress}"
      TTL="200"

      ZONE=`hostname`.${var.dns_zone}
      IPADDR=`hostname -I | awk '{print $1;}'`

      (
        echo "server $NAMESERVER"
        echo "update delete ubuntu.${var.dns_zone} A"
        echo "update delete $ZONE A"
        echo "update add $ZONE $TTL A $IPADDR"
        echo "send"
      ) | nsupdate -k /opt/rndc.key

  - path: /etc/modules-load.d/kubernetes.conf
    content: |
      overlay
      br_netfilter

  - path: /etc/sysctl.d/kubernetes.conf
    content: | 
      net.bridge.bridge-nf-call-iptables  = 1
      net.bridge.bridge-nf-call-ip6tables = 1
      net.ipv4.ip_forward                 = 1

power_state:
    delay: 30
    mode: reboot
    message: Rebooting after cloud-init completion
    condition: true  
EOT
}


resource "local_file" "additional_control_plane_node_snippet" {
  filename        = var.additional_control_plane_node_snippet.path
  file_permission = "666"
  content         = <<-EOT
#cloud-config
package_upgrade: true
packages:
  - qemu-guest-agent
  - htop
  - git
  - mc
  - ca-certificates
  - curl
  - gnupg-agent
  - software-properties-common
  - open-iscsi
  - nfs-common
  - gnupg2 
  - apt-transport-https 
  - lsb-release
  - python3-pip
  - blkid
  - findmnt

timezone: ${var.timezone}
users:
  - name: ${var.cloud_init_user.name}
    gecos: ${var.cloud_init_user.gecos}
    sudo: 'ALL=(ALL) NOPASSWD:ALL'
    groups: [users, admin, docker]
    passwd: ${var.cloud_init_user.password}
    shell: /bin/bash
    lock-passwd: false
    ssh_pwauth: true
    ssh_authorized_keys:
        %{~for keys in var.cloud_init_user.ssh_keys~}
        - ${keys};
        %{~endfor~}
ca_certs:
  remove_defaults: false
  trusted:
  - |
${var.trusted_certs_bundle}

runcmd:
  - |
    mkdir -p /etc/systemd/resolved.conf.d/
    echo "[Resolve]" > /etc/systemd/resolved.conf.d/dns_servers.conf
    echo "DNS=${var.master_bind9_node.bind9_master.network.ipv4}" >> /etc/systemd/resolved.conf.d/dns_servers.conf
    systemctl restart systemd-resolved  
    mkdir -p /etc/apt/keyrings
    [ ! -f /etc/apt/keyrings/docker.gpg ] && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    [ ! -f /etc/apt/sources.list.d/docker.list ] && echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

    curl -fsSL https://pkgs.k8s.io/core:/stable:/v${local.kubernetes_version_minor}/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
    echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v${local.kubernetes_version_minor}/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
    ln -s /usr/bin/kubectl /usr/local/bin/k
    apt update
    apt install -y containerd.io 
    apt install -y qemu-guest-agent
    systemctl enable qemu-guest-agent 
    systemctl start qemu-guest-agent
    containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1
    sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
    systemctl restart containerd
    systemctl enable containerd
    hostnamectl set-hostname k8s-controller-$(openssl rand -hex 3)
    apt install -y kubelet=${var.kubernetes_version} kubeadm=${var.kubernetes_version} kubectl=${var.kubernetes_version}
    apt-mark hold kubelet kubeadm kubectl
    modprobe br_netfilter
    modprobe overlay
    sleep 15
    echo 1 > /proc/sys/net/bridge/bridge-nf-call-iptables
    echo 1 > /proc/sys/net/ipv4/ip_forward
    kubeadm join master.${var.dns_zone}:6443 --token ${var.kubernetes_settings.token} \
    --discovery-token-unsafe-skip-ca-verification \
    --control-plane --certificate-key ${var.kubernetes_settings.certificate_key}
    mkdir -p /home/${var.cloud_init_user.name}/.kube
    cp /etc/kubernetes/admin.conf /home/${var.cloud_init_user.name}/.kube/config
    chown -R ${var.cloud_init_user.name}:${var.cloud_init_user.name} /home/${var.cloud_init_user.name}/.kube


#cloud-config
write_files:
  - path: /etc/cron.d/ddns_update
    content: |
      */5 * * * * root bash /opt/ddns-update


  - path: /opt/rndc.key
    content: |
     key "rndc-key" {
      algorithm ${var.dns_rndc_key.tsigAlgorithm};
      secret "${var.dns_rndc_key.tsigSecret}";
     };

  - path: /opt/ddns-update
    permissions: '0755'
    content: |
      KEYFILE=/opt/rndc.key
      NAMESERVER="${var.dns_ipaddress}"
      TTL="200"

      ZONE=`hostname`.${var.dns_zone}
      IPADDR=`hostname -I | awk '{print $1;}'`

      (
        echo "server $NAMESERVER"
        echo "update delete ubuntu.${var.dns_zone} A"
        echo "update delete $ZONE A"
        echo "update add $ZONE $TTL A $IPADDR"
        echo "send"
      ) | nsupdate -k /opt/rndc.key

  - path: /etc/modules-load.d/kubernetes.conf
    content: |
      overlay
      br_netfilter

  - path: /etc/sysctl.d/kubernetes.conf
    content: | 
      net.bridge.bridge-nf-call-iptables  = 1
      net.bridge.bridge-nf-call-ip6tables = 1
      net.ipv4.ip_forward                 = 1

power_state:
    delay: now
    mode: reboot
    message: Rebooting after cloud-init completion
    condition: true
EOT
}


resource "local_file" "worker_node_snippet" {
  filename        = var.worker_node_snippet.path
  file_permission = "666"
  content         = <<-EOT
#cloud-config
package_upgrade: true
packages:
  - qemu-guest-agent
  - htop
  - git
  - mc
  - ca-certificates
  - curl
  - gnupg-agent
  - software-properties-common
  - open-iscsi
  - nfs-common
  - ansible
  - gnupg2 
  - apt-transport-https 
  - lsb-release
  - python3-pip
  - blkid
  - findmnt

timezone: ${var.timezone}
users:
  - name: ${var.cloud_init_user.name}
    gecos: ${var.cloud_init_user.gecos}
    sudo: 'ALL=(ALL) NOPASSWD:ALL'
    groups: [users, admin, docker]
    passwd: ${var.cloud_init_user.password}
    shell: /bin/bash
    lock-passwd: false
    ssh_pwauth: true
    ssh_authorized_keys:
        %{~for keys in var.cloud_init_user.ssh_keys~}
        - ${keys};
        %{~endfor~}

ca_certs:
  remove_defaults: false
  trusted:
  - |
${var.trusted_certs_bundle}

bootcmd:
 - |
   test -z "$(blkid /dev/sdb)" && printf "o\nn\np\n1\n\n\nw\n" | sudo fdisk /dev/sdb
mounts:
 - [ "/dev/sdb1", "/var/lib/longhorn", "xfs", "defaults,nofail", "0", "2" ]
runcmd:
  - |
    mkdir -p /etc/systemd/resolved.conf.d/
    echo "[Resolve]" > /etc/systemd/resolved.conf.d/dns_servers.conf
    echo "DNS=${var.master_bind9_node.bind9_master.network.ipv4}" >> /etc/systemd/resolved.conf.d/dns_servers.conf
    systemctl restart systemd-resolved
    mkfs.xfs /dev/sdb1 && mount /dev/sdb1
    mkdir -p /etc/apt/keyrings
    [ ! -f /etc/apt/keyrings/docker.gpg ] && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    [ ! -f /etc/apt/sources.list.d/docker.list ] && echo \
    "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null

    curl -fsSL https://pkgs.k8s.io/core:/stable:/v${local.kubernetes_version_minor}/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
    echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v${local.kubernetes_version_minor}/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list
    ln -s /usr/bin/kubectl /usr/local/bin/k
    apt update
    apt install -y qemu-guest-agent
    systemctl enable qemu-guest-agent 
    systemctl start qemu-guest-agent
    apt install -y containerd.io 
    containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1
    sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml
    systemctl restart containerd
    systemctl enable containerd
    hostnamectl set-hostname k8s-worker-$(openssl rand -hex 3)
    apt install -y kubelet=${var.kubernetes_version} kubeadm=${var.kubernetes_version} kubectl=${var.kubernetes_version}
    apt-mark hold kubelet kubeadm kubectl
    modprobe br_netfilter
    modprobe overlay
    sleep 15
    echo 1 > /proc/sys/net/bridge/bridge-nf-call-iptables
    echo 1 > /proc/sys/net/ipv4/ip_forward
    sleep 900
    kubeadm join master.${var.dns_zone}:6443 --token ${var.kubernetes_settings.token} \
    --discovery-token-unsafe-skip-ca-verification


#cloud-config
write_files:
  - path: /etc/cron.d/ddns_update
    content: |
      */5 * * * * root bash /opt/ddns-update


  - path: /opt/rndc.key
    content: |
     key "rndc-key" {
      algorithm ${var.dns_rndc_key.tsigAlgorithm};
      secret "${var.dns_rndc_key.tsigSecret}";
     };

  - path: /opt/ddns-update
    permissions: '0755'
    content: |
      KEYFILE=/opt/rndc.key
      NAMESERVER="${var.dns_ipaddress}"
      TTL="200"

      ZONE=`hostname`.${var.dns_zone}
      IPADDR=`hostname -I | awk '{print $1;}'`

      (
        echo "server $NAMESERVER"
        echo "update delete ubuntu.${var.dns_zone} A"
        echo "update delete $ZONE A"
        echo "update add $ZONE $TTL A $IPADDR"
        echo "send"
      ) | nsupdate -k /opt/rndc.key

  - path: /etc/modules-load.d/kubernetes.conf
    content: |
      overlay
      br_netfilter

  - path: /etc/sysctl.d/kubernetes.conf
    content: | 
      net.bridge.bridge-nf-call-iptables  = 1
      net.bridge.bridge-nf-call-ip6tables = 1
      net.ipv4.ip_forward                 = 1

power_state:
    delay: now
    mode: reboot
    message: Rebooting after cloud-init completion
    condition: true
EOT
}