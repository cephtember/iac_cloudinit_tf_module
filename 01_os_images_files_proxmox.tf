resource "proxmox_virtual_environment_file" "ubuntu_2204_cloud_image" {
  content_type = "iso"
  datastore_id = var.ubuntu_2204_cloud_image.datastore_id
  node_name    = var.ubuntu_2204_cloud_image.node_name
  overwrite    = false

  source_file {
    path     = var.ubuntu_2204_cloud_image.image_link
    insecure = true
  }

  timeout_upload = 3600
}