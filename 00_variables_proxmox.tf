variable "proxmox_login" {}
variable "proxmox_password" {}
variable "proxmox_endpoint" {}

variable "ubuntu_2204_cloud_image" {
  type = map(any)
  default = {
    datastore_id = "local"
    node_name    = "home"
    image_link   = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img"
  }
}

variable "kubernetes_version" {
  type    = string
  default = "1.28.4-1.1"
}

variable "k8s_master_ipaddress_ha" {
  type    = string
  default = "10.23.31.70"
}

variable "timezone" {
  type    = string
  default = "Asia/Tashkent"
}

variable "kubernetes_settings" {
  type = object({
    master_ipaddress_0   = string
    master_ipaddress_1   = string
    master_ipaddress_2   = string
    pod_network_cidr     = string
    service_network_cidr = string
    token                = string
    certificate_key      = string
  })
  default = {
    master_ipaddress_0   = "10.23.31.65"
    master_ipaddress_1   = "10.23.31.66"
    master_ipaddress_2   = "10.23.31.67"
    pod_network_cidr     = "10.233.0.0/16"
    service_network_cidr = "10.234.0.0/16"
    token                = "terraf.9981523290230991"
    certificate_key      = "7a9161994b226ae2328f4df7d132243bec76498f890a2c5c4504c5e99463ffad"
  }
}

variable "cloud_init_user" {
  description = "name = username (string), gecos = fullName (string), password = (echo password | mkpasswd -m sha-512 -s)"
  type = object({
    name     = string
    password = string
    gecos    = string
    ssh_keys = list(string)
  })
  default = {
    name     = "cepht"
    password = "$6$Jcs4OkyckkJusXl9$oMOVKzi8wmboyw8x7koSMIrRwEZK474W.oVit3H1Vogu1lwmBiolumS6JIGfncmrZgFZqjRyi3mfzMnDQmuf90"
    gecos    = "Vladimir O. Filatov"
    ssh_keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDevVe68xWIcZwg3+rM8MasGO40Uz3VFTrpfyHmEwVxdIMePNcDWaU20SlPbeLV/EyGpGy3ipwEWhSAEPouTxDEoPL8rhnQ1AwZNLQMaixG1bT7OV53WTO/0gVA7xwZb+fT6TmO/fnOc0Kjnp3UwF3bFQHfUo+IwxDOTr3ZmuU8IjVmdKa+1qFdRjAnpVZQzLkC42nAiYgty+nrCVyYD1OcDLSE+ZdLVJuSiiKIKCMKyxlpnPH95WsrhSWCBLUtXAleheLDK6MAAW55JLrKYclelSsc+swrCeI+d1tr3iGbzp+C+BhWFcnEH9Vfkfbd0YhXerla6WgVEF0GtV8a8h6BwHlvDe7+3awLau6ak+bZHBZxTfe6xOgBcDSyMww1VgIKXbeNRmvLDmT4+yVg+koqFblIKuYa1KuIaLZIVL4Xwyyx9nskd09NrJ7kLpkbm8Wwy/Oi0svhG2BvQWmdrdx4lT+Ps/u34+I1OI2FJsqyHWlacVCd3YeW7NvzWyXzckpQVRSDpwh6zx0zw4mnsVJ0nePFS2ENt+ctRQUon3QKsbz9RnN5W09mpKumshTnjfKbs8j6Z68kG9M1vWt7oucqV9emElKxMGX/4sc3sh31cneqbIliqnEa0ADIRGBkSGaCrLMSbOZN8Nwf7qOIhA3WJXRYjW13qr84as4LugWCgw==  openpgp",
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnzCuKuANdtfnLunxPn0sWVotGdImulT74vzyuyFxeXVGxENlxVEmu3436kNRSOEAk0ch9nH5Tf469xnFmtxDYFA+80hZp2HXPO1DpcY60RDgIl7oDsdhxIgYMpNeZyINZqtITzmb/CVbwu47sgrm2C+exsfRA7ehZK48mL82EiHE6lH6lxYvbHhabYKGUEi+RODsaeuHLKa4ljLqik0YaipNp8k2e/fHKlWSYJNsK1OfkwUEfgqyQunGbpvoeg14X4JeA1rgiIfJiIq4xPcxITRRQxWe4bZGZcBfb9q6tqKrjCRVoc0RDXqaHHM2S9NctpWSn9kvL+D6H/NhqxZol PIV AUTH pubkey",
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDTqFbVO9uKA0r+gVb9Q6Q57XtE/i/dljBCWd2ZUMgd3LZxQIAyenC/0ep6817nXb+VP56sW70mhVM97pQFOANo7Ysrdn3f/TDdrug2P/sYshUQf6D8Gbm82XqhshM6dP6uAcXvNwXaXKEMhiNHRlYxs2Ta9SfBsmRx2zCcNDeIJ6eaQQ7LI/m8utkW2dfhFITVAuXZU3MO0FkrgEJKEjRhBbCyENkkGlvXnxN3gjcwxDkAlahzBPv6Wf0NrzDjlmGcbIqyqy+xxelUsJuCrkmQHVZBsQ0vCGb+A7s22ZAE/lCBu6A2SWYoK2xPKlt65+dUGpxskbMWkP76yzRHChK0LPxDfon3h/fe3IPdlzWn/YNkKn1SIazX+0GDhQQHYFMxTtWku/MfSKLG9NtYnmNYI0YmIhVkqgzcZMal0Kag7tO+X0ST4xl6cIkA/S6t0WgpCEiptT5I1EdCoJWa52el2yemdWDcdacpBa+87PD0/fnbTCm9hetzA+9tTyYOSqU= cepht@macBookamneZia.local"
    ]
  }
}

variable "trusted_certs_bundle" {
  description = "The trusted certificate"
  type        = string
  default     = <<EOT
    -----BEGIN CERTIFICATE-----
    MIIDjjCCAnagAwIBAgIQAzrx5qcRqaC7KGSxHQn65TANBgkqhkiG9w0BAQsFADBh
    MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3
    d3cuZGlnaWNlcnQuY29tMSAwHgYDVQQDExdEaWdpQ2VydCBHbG9iYWwgUm9vdCBH
    MjAeFw0xMzA4MDExMjAwMDBaFw0zODAxMTUxMjAwMDBaMGExCzAJBgNVBAYTAlVT
    MRUwEwYDVQQKEwxEaWdpQ2VydCBJbmMxGTAXBgNVBAsTEHd3dy5kaWdpY2VydC5j
    b20xIDAeBgNVBAMTF0RpZ2lDZXJ0IEdsb2JhbCBSb290IEcyMIIBIjANBgkqhkiG
    9w0BAQEFAAOCAQ8AMIIBCgKCAQEAuzfNNNx7a8myaJCtSnX/RrohCgiN9RlUyfuI
    2/Ou8jqJkTx65qsGGmvPrC3oXgkkRLpimn7Wo6h+4FR1IAWsULecYxpsMNzaHxmx
    1x7e/dfgy5SDN67sH0NO3Xss0r0upS/kqbitOtSZpLYl6ZtrAGCSYP9PIUkY92eQ
    q2EGnI/yuum06ZIya7XzV+hdG82MHauVBJVJ8zUtluNJbd134/tJS7SsVQepj5Wz
    tCO7TG1F8PapspUwtP1MVYwnSlcUfIKdzXOS0xZKBgyMUNGPHgm+F6HmIcr9g+UQ
    vIOlCsRnKPZzFBQ9RnbDhxSJITRNrw9FDKZJobq7nMWxM4MphQIDAQABo0IwQDAP
    BgNVHRMBAf8EBTADAQH/MA4GA1UdDwEB/wQEAwIBhjAdBgNVHQ4EFgQUTiJUIBiV
    5uNu5g/6+rkS7QYXjzkwDQYJKoZIhvcNAQELBQADggEBAGBnKJRvDkhj6zHd6mcY
    1Yl9PMWLSn/pvtsrF9+wX3N3KjITOYFnQoQj8kVnNeyIv/iPsGEMNKSuIEyExtv4
    NeF22d+mQrvHRAiGfzZ0JFrabA0UWTW98kndth/Jsw1HKj2ZL7tcu7XUIOGZX1NG
    Fdtom/DzMNU+MeKNhJ7jitralj41E6Vf8PlwUHBHQRFXGU7Aj64GxJUTFy8bJZ91
    8rGOmaFvE7FBcf6IKshPECBV1/MUReXgRPTqh5Uykw7+U0b6LJ3/iyK5S9kJRaTe
    pLiaWN0bfVKfjllDiIGknibVb63dDcY3fe0Dkhvld1927jyNxF1WW6LZZm6zNTfl
    MrY=
    -----END CERTIFICATE-----
    EOT
}

##### ДНС #####

variable "dns_ipaddress" {
  type    = string
  default = "10.23.31.69"
}

variable "dns_zone" {
  type    = string
  default = "k8s.loc"
}

variable "dns_acl_good_client" {
  type        = list(string)
  description = "list of whitelisted ipaddresses"
  default     = ["10.23.31.0/24", "10.233.0.0/16", "10.234.0.0/16"]
}

variable "dns_forwarders" {
  type        = list(string)
  description = "list of dns forwarders"
  default     = ["8.8.8.8", "8.8.4.4"]
}

variable "dns_rndc_key" {
  type = object({
    tsigSecret    = string
    tsigAlgorithm = string
  })
  default = {
    tsigSecret    = "SHgLMsmVE+HsapBekveY3NBsbE8PmLA01MSJti/OjA8="
    tsigAlgorithm = "hmac-sha256"
  }
}

variable "master_bind9_node_snippet" {
  type = map(any)
  default = {
    datastore_id = "local"
    node_name    = "home"
    path         = "files/cloudinit_userdata_files/master_bind9_node_snippet.yml"
  }
}

variable "master_bind9_node" {
  description = "Master_Bind9 - Node"
  type        = map(any)
  default = {
    bind9_master = {
      name   = "bind9-master-0"
      tags   = ["bind9", "terraform", "for_k8s"]
      memory = 1024
      cpu    = 1
      disk   = 16
      network = {
        bridge    = "vmbr0"
        dns       = "10.23.31.200"
        ipv4      = "10.23.31.69"
        ipv4_full = "10.23.31.69/24"
        gateway   = "10.23.31.200"
      }
      datastore_id = "local-lvm"
      node_name    = "home"
    },
  }
}


variable "master_loadbalancer_node_snippet" {
  type = map(any)
  default = {
    datastore_id = "local"
    node_name    = "home"
    path         = "files/cloudinit_userdata_files/master_loadbalancer_node_snippet.yml"
  }
}

variable "master_loadbalancer_node" {
  description = "Master_LB - Node"
  type        = map(any)
  default = {
    lb_master_node = {
      name   = "k8s-controller-lb"
      tags   = ["loadbalancer", "terraform", "k8s"]
      memory = 1024
      cpu    = 1
      disk   = 16
      network = {
        bridge    = "vmbr0"
        dns       = "10.23.31.69"
        ipv4      = "10.23.31.70"
        ipv4_full = "10.23.31.70/24"
        gateway   = "10.23.31.200"
      }
      datastore_id = "local-lvm"
      node_name    = "home"
    },
  }
}


variable "master_control_plane_node_snippet" {
  type = map(any)
  default = {
    datastore_id = "local"
    node_name    = "home"
    path         = "files/cloudinit_userdata_files/master_control_plane_node_snippet.yml"
  }
}

variable "master_control_plane_node" {
  description = "Master_ControlPlane - Node"
  type        = map(any)
  default = {
    master_plane_node = {
      name   = "k8s-controller-0"
      tags   = ["initial", "controlplane", "terraform", "k8s"]
      memory = 4096
      cpu    = 4
      disk   = 16
      network = {
        bridge    = "vmbr0"
        dns       = "10.23.31.69"
        ipv4      = "10.23.31.65"
        ipv4_full = "10.23.31.65/24"
        gateway   = "10.23.31.200"
      }
      datastore_id = "local-lvm"
      node_name    = "home"
    },
  }
}


variable "additional_control_plane_node_snippet" {
  type = map(any)
  default = {
    datastore_id = "local"
    node_name    = "home"
    path         = "files/cloudinit_userdata_files/additional_control_plane_node_snippet.yml"
  }
}

variable "additional_control_plane_nodes" {
  description = "Additional_ControlPlane - Nodes"
  type        = map(any)
  default     = {}
}


variable "worker_node_snippet" {
  type = map(any)
  default = {
    datastore_id = "local"
    node_name    = "home"
    path         = "files/cloudinit_userdata_files/worker_node_snippet.yml"
  }
}

variable "master_worker_node" {
  description = "Master_Worker - Node"
  type        = map(any)
  default = {
    worker_node_0 = {
      name   = "k8s-worker-0"
      tags   = ["initial", "worker", "terraform", "k8s"]
      memory = 2048
      cpu    = 2
      disk   = 16
      disk2  = 0
      network = {
        bridge    = "vmbr0"
        dns       = "10.23.31.69"
        ipv4      = "10.23.31.71"
        ipv4_full = "10.23.31.71/24"
        gateway   = "10.23.31.200"
      }
      datastore_id = "local-lvm"
      node_name    = "home"
    },
  }
}


variable "additional_worker_nodes" {
  description = "Additional_Worker - Node"
  type        = map(any)
  default     = {}
}

