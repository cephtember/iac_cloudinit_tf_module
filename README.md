# Модуль terraform для развертывания kubernetes средствами kubeadm через cloudinit

Модуль используется в качестве личных экспериментов и сделан просто ради фана.

> На самом деле я просто устал пересобирать
> себе кластер для тестирования каких либо
> сценариев / чартов / приложений.

## TODO
- Доработать модуль под Nutanix
- Доработать модуль для VMWare vCenter
- Сделать возможность выбора системы для установки (Proxmox, Nutanix, vCenter)
- Сделать возможность установки кластера в мультивиртуализации
- Сделать возможность смены datastore_id при уставновке в ProxMox

## Features

- Установка k8s кластера с HAProxy для control plane нод 
- Установка bind9 сервера с автообновлением dns записей
- Добавление / Удаление дополнительных пулов нод

## Installation
  
  Устанавливаем необходимые приложения и зависимости
  
  Macbook M1 Fix:
  ```
  brew uninstall terraform
  brew install tfenv
  TFENV_ARCH=amd64 tfenv install 1.4.6
  tfenv use 1.4.6
  ```

  Приводим terraform main.tf фаил к следующему виду

```
module "cloud_init" {
#  Путь до модуля (может быть как git repo или путь до папки)
#  
  source = "git@gitlab.com:cephtember/iac_cloudinit_tf_module.git"

#  Логин от привилегированного пользователя
#
  proxmox_login = "root@pam"

#  Пароль от привилигированного пользователя
#
  proxmox_password = "password"

#  Ссылка до ProxMox Api
#
  proxmox_endpoint = "https://127.0.0.1:8006/api2/json"


#  Ссылка на скачивание cloud_init образа (Optional)
#
#  ubuntu_2204_cloud_image = {
#     datastore_id = "local" #[default: local]
#     node_name    = "home" #[default: home]
#     image_link = "https://cloud-images.ubuntu.com/jammy/current/jammy-server-cloudimg-amd64.img"
#
#  }


#  Используемая версия k8s для установки (Optional)
#
#  kubernetes_version = "1.29.2-1.1" #[default: 1.28.4-1.1]


#  IP Address для HAProxy (Optional)
#  (Не распространяется на Виртуальные машины)
#
#  k8s_master_ipaddress_ha = "10.23.31.70" #[default: 10.23.31.70]


#  TimeZone для системы
#
#  timezone = "Asia/Tashkent" #[default: Asia/Tashkent]


#  Основные настройки для Kubernetes (Optional)
#  (Не распространяется на Виртуальные машины)
#
#  kubernetes_settings = {
#    master_ipaddress_0   = "10.23.31.65" #[default: 10.23.31.65]
#    master_ipaddress_1   = "10.23.31.66" #[default: 10.23.31.66]
#    master_ipaddress_2   = "10.23.31.67" #[default: 10.23.31.67]
#    pod_network_cidr     = "10.233.0.0/16" #[default: 10.233.0.0/16]
#    service_network_cidr = "10.234.0.0/16" #[default: 10.234.0.0/16]
#    token                = "terraf.9981523290230991" 
#    certificate_key      = "7a9161994b226ae2328f4df7d132243bec76498f890a2c5c4504c5e99463ffad"
#  }


#  Пользователь для Виртуальных машин (Optional)
#  для генерации строки пароля  ```password = (echo password | mkpasswd -m sha-512 -s)```
#
#  cloud_init_user = {
#    name     = "cepht"
#    password = "$6$Jcs4OkyckkJusXl9$oMOVKzi8wmboyw8x7koSMIrRwEZK474W.oVit3H1Vogu1lwmBiolumS6JIGfncmrZgFZqjRyi3mfzMnDQmuf90"
#    gecos    = "Vladimir O. Filatov"
#    ssh_keys = [
#      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDevVe68xWIcZwg3+rM8MasGO40Uz3VFTrpfyHmEwVxdIMePNcDWaU20SlPbeLV/EyGpGy3ipwEWhSAEPouTxDEoPL8rhnQ1AwZNLQMaixG1bT7OV53WTO/0gVA7xwZb+fT6TmO/fnOc0Kjnp3UwF3bFQHfUo+IwxDOTr3ZmuU8IjVmdKa+1qFdRjAnpVZQzLkC42nAiYgty+nrCVyYD1OcDLSE+ZdLVJuSiiKIKCMKyxlpnPH95WsrhSWCBLUtXAleheLDK6MAAW55JLrKYclelSsc+swrCeI+d1tr3iGbzp+C+BhWFcnEH9Vfkfbd0YhXerla6WgVEF0GtV8a8h6BwHlvDe7+3awLau6ak+bZHBZxTfe6xOgBcDSyMww1VgIKXbeNRmvLDmT4+yVg+koqFblIKuYa1KuIaLZIVL4Xwyyx9nskd09NrJ7kLpkbm8Wwy/Oi0svhG2BvQWmdrdx4lT+Ps/u34+I1OI2FJsqyHWlacVCd3YeW7NvzWyXzckpQVRSDpwh6zx0zw4mnsVJ0nePFS2ENt+ctRQUon3QKsbz9RnN5W09mpKumshTnjfKbs8j6Z68kG9M1vWt7oucqV9emElKxMGX/4sc3sh31cneqbIliqnEa0ADIRGBkSGaCrLMSbOZN8Nwf7qOIhA3WJXRYjW13qr84as4LugWCgw==  openpgp",
#      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCnzCuKuANdtfnLunxPn0sWVotGdImulT74vzyuyFxeXVGxENlxVEmu3436kNRSOEAk0ch9nH5Tf469xnFmtxDYFA+80hZp2HXPO1DpcY60RDgIl7oDsdhxIgYMpNeZyINZqtITzmb/CVbwu47sgrm2C+exsfRA7ehZK48mL82EiHE6lH6lxYvbHhabYKGUEi+RODsaeuHLKa4ljLqik0YaipNp8k2e/fHKlWSYJNsK1OfkwUEfgqyQunGbpvoeg14X4JeA1rgiIfJiIq4xPcxITRRQxWe4bZGZcBfb9q6tqKrjCRVoc0RDXqaHHM2S9NctpWSn9kvL+D6H/NhqxZol PIV AUTH pubkey",
#      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDTqFbVO9uKA0r+gVb9Q6Q57XtE/i/dljBCWd2ZUMgd3LZxQIAyenC/0ep6817nXb+VP56sW70mhVM97pQFOANo7Ysrdn3f/TDdrug2P/sYshUQf6D8Gbm82XqhshM6dP6uAcXvNwXaXKEMhiNHRlYxs2Ta9SfBsmRx2zCcNDeIJ6eaQQ7LI/m8utkW2dfhFITVAuXZU3MO0FkrgEJKEjRhBbCyENkkGlvXnxN3gjcwxDkAlahzBPv6Wf0NrzDjlmGcbIqyqy+xxelUsJuCrkmQHVZBsQ0vCGb+A7s22ZAE/lCBu6A2SWYoK2xPKlt65+dUGpxskbMWkP76yzRHChK0LPxDfon3h/fe3IPdlzWn/YNkKn1SIazX+0GDhQQHYFMxTtWku/MfSKLG9NtYnmNYI0YmIhVkqgzcZMal0Kag7tO+X0ST4xl6cIkA/S6t0WgpCEiptT5I1EdCoJWa52el2yemdWDcdacpBa+87PD0/fnbTCm9hetzA+9tTyYOSqU= cepht@macBookamneZia.local"
#    ]
#  }


# CA Bundle Trusted Сертификаты для добавления в доверенные корневые (Optional)
#
# trusted_certs_bundle = <<EOT
#    -----BEGIN CERTIFICATE-----
#    MIIDazCCAlOgAwIBAgIQZ1P6czlKVK5LFcZvgfkPnjANBgkqhkiG9w0BAQsFADBI
#    MRMwEQYKCZImiZPyLGQBGRYDbG9jMRIwEAYKCZImiZPyLGQBGRYCYWQxHTAbBgNV
#    BAMTFGFkLUNMSy1DQS1TUlYtMDAxLUNBMB4XDTIyMTIwNTA2NDIzMFoXDTQyMTIw
#    NTA2NTIzMFowSDETMBEGCgmSJomT8ixkARkWA2xvYzESMBAGCgmSJomT8ixkARkW
#    AmFkMR0wGwYDVQQDExRhZC1DTEstQ0EtU1JWLTAwMS1DQTCCASIwDQYJKoZIhvcN
#    AQEBBQADggEPADCCAQoCggEBALrqdLYLkskk/NdKziParznxblVI6T1bJPmxnSgz
#    MPyw7BeculJkJAIi8r343JIzzoYV8wM888XfqgWPPM4hrRgGB0n5VDQVU7DR+dyi
#    iqiE0a1xgz43MapQ41LwrPSlz1DnHdGFr6brfNRnUFNH+g7fPm38Xg/rzl8C4P/q
#    tPVH+3SyVnJ33fuH4GAXkicXwJJVhnWW6EzTInz8RwWRScnZQ5JFIGqEKv/hgmnc
#    6YSoV+nMlgFfGiJoRfnZKn1Zb9QJwKS5NTI6NATsMra0RQasNwmetvzrb0v3P9DT
#    kM8Ifu5pOBE8T/Za22piChV2CvBCfpPfxcQogGz7cC1NN1kCAwEAAaNRME8wCwYD
#    VR0PBAQDAgGGMA8GA1UdEwEB/wQFMAMBAf8wHQYDVR0OBBYEFLjL7dtCwzkZDhn2
#    xGttYAAGc+pNMBAGCSsGAQQBgjcVAQQDAgEAMA0GCSqGSIb3DQEBCwUAA4IBAQAm
#    aZrbM2kCjlx35BsgtJxGXQJbdW5CHIO3+Uqf7k6g4r3wMRf45/hIpIHdvQW0F060
#    zL3bWvNHkjmANHz+0fcUVxNFL34kJwHHB4cpYX6kC3+JfjShWuW5eTpOY/t5Yb2k
#    03dUcPqUX2vl7UAcV47TJZfp1EhjPLnmS2vNhN+20Z441SS3l4gaXhJvorqBstiV
#    ij8O+VEXWj+TXs2Q34oPZEH289FydFfdOfYa2zpQ98kbAsqJ6R/U/nCDz+TbEO20
#    e2br3HmPOfxFBFCfpiQxN6pAeGOyy+1GUY3ySK0hjGn0KrVGjTPghH0/FAP9Dk3Y
#    gD7WUdNGG9PvBgTUwBBd
#    -----END CERTIFICATE-----
#    EOT


#  IP Address DNS сервера (Optional)
#  (Не распространяется на Виртуальные машины)
#
#  dns_ipaddress = "10.23.31.69" #[default: 10.23.31.69]


#  DNS зона, которая будет использоваться для k8s кластера (external)
# 
#  dns_zone = "k8s.loc" #[default: k8s.loc]


#  Разрешенные сети для обращений к DNS (Optional)
#
#  dns_acl_good_client = ["10.23.31.0/24", "10.233.0.0/16", "10.234.0.0/16"]


#  DNS Forwarders (Optional)
#
#  dns_forwarders = ["8.8.8.8", "8.8.4.4"] #[default: 8.8.8.8, 8.8.4.4]


#  DNS ключ для динамического обновления записей (Optional)
#
#  dns_rndc_key = {
#    tsigSecret    = "SHgLMsmVE+HsapBekveY3NBsbE8PmLA01MSJti/OjA8=" #[default: SHgLMsmVE+Hsa...]
#    tsigAlgorithm = "hmac-sha256" #[default: hmac-sha256]
#  }


#  Описание дополнительных MASTER нод для добавления в кластер
#
#  additional_control_plane_nodes = {
#    master_plane_node_1 = {
#    name = "k8s-controller-1"
#    tags   = ["controlplane", "terraform", "k8s"]
#    memory = 4096
#    cpu    = 4
#    disk   = 16
#    network = {
#      bridge  = "vmbr0"
#      dns     = "10.23.31.69"
#      ipv4    = "10.23.31.66"
#      ipv4_full = "10.23.31.66/24"
#      gateway = "10.23.31.200"
#    }
#    datastore_id = "local-lvm"
#    node_name    = "home"
#    },
#    master_plane_node_2 = {
#      ...
#      ...
#    },
#  }


#  Описание дополнительных Worker Node для добавления в кластер
#  additional_worker_nodes = {
#    worker_node_1 = {
#      name   = "k8s-worker-1"
#      tags   = ["worker", "terraform", "k8s"]
#      memory = 2048
#      cpu    = 2
#      disk   = 16
#      disk2  = 0 #Используется для longhorn, минимальное значение 1
#      network = {
#        bridge  = "vmbr0"
#        dns     = "10.23.31.69"
#        ipv4    = "10.23.31.72"
#        ipv4_full  = "10.23.31.72/24"
#        gateway = "10.23.31.200"
#      }
#      datastore_id = "local-lvm"
#      node_name    = "home"
#    },
#    worker_node_2 = {
#      ...
#      ...
#    }, 
#  }


##### [--- ADVANCED ---] #####

#
#  master_bind9_node = {
#    bind9_master = {
#      name   = "bind9-master-0"
#      tags   = ["bind9", "terraform", "for_k8s"]
#      memory = 1024
#      cpu    = 1
#      disk   = 16
#      network = {
#        bridge  = "vmbr0"
#        dns     = "10.23.31.200"
#        ipv4    = "10.23.31.69"
#        ipv4_full = "10.23.31.69/24"
#        gateway = "10.23.31.200"
#      }
#      datastore_id = "local-lvm"
#      node_name    = "home"
#    },
#  }

#
#  master_loadbalancer_node = {
#    lb_master_node = {
#      name   = "k8s-controller-lb"
#      tags   = ["loadbalancer", "terraform", "k8s"]
#      memory = 1024
#      cpu    = 1
#      disk   = 16
#      network = {
#        bridge  = "vmbr0"
#        dns     = "10.23.31.69"
#        ipv4    = "10.23.31.70"
#        ipv4_full = "10.23.31.70/24"
#        gateway = "10.23.31.200"
#      }
#      datastore_id = "local-lvm"
#      node_name    = "home"
#    },
#  }

#
#  master_control_plane_node = {
#    master_plane_node = {
#      name = "k8s-controller-0"
#      tags   = ["initial", "controlplane", "terraform", "k8s"]
#      memory = 4096
#      cpu    = 4
#      disk   = 16
#      network = {
#        bridge  = "vmbr0"
#        dns     = "10.23.31.69"
#        ipv4    = "10.23.31.65"
#        ipv4_full = "10.23.31.65/24"
#        gateway = "10.23.31.200"
#      }
#      datastore_id = "local-lvm"
#      node_name    = "home"
#    },
#  }

#
#  master_worker_node = {
#    worker_node_0 = {
#      name   = "k8s-worker-0"
#      tags   = ["initial", "worker", "terraform", "k8s"]
#      memory = 2048
#      cpu    = 2
#      disk   = 16
#      disk2  = 0 #Используется для longhorn, минимальное значение 1
#      network = {
#        bridge  = "vmbr0"
#        dns     = "10.23.31.69"
#        ipv4    = "10.23.31.71"
#        ipv4_full = "10.23.31.71/24"
#        gateway = "10.23.31.200"
#      }
#      datastore_id = "local-lvm"
#      node_name    = "home"
#    },
#  }
}
```


## Plugins

| Plugin | README |
| ------ | ------ |
| 00_Main Module | [https://gitlab.com/cephtember/iac_tf_project] |
| 01_CloudInit Module ProxMox | [https://gitlab.com/cephtember/iac_cloudinit_tf_module_proxmox] |
| 01_CloudInit Module Nutanix | [https://gitlab.com/cephtember/iac_cloudinit_tf_module_nutanix] |
| 02_Core Module | [https://gitlab.com/cephtember/iac_core_k8s_tf_module] |
| 03_Security Module | [https://gitlab.com/cephtember/iac_security_k8s_tf_module] |