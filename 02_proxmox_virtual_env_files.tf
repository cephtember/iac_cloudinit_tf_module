resource "proxmox_virtual_environment_file" "master_bind9_node_snippet" {
  content_type = "snippets"
  datastore_id = var.master_bind9_node_snippet.datastore_id
  node_name    = var.master_bind9_node_snippet.node_name

  source_file {
    path = local_file.master_bind9_node_snippet.filename
  }

  depends_on = [
    proxmox_virtual_environment_file.ubuntu_2204_cloud_image,
    local_file.master_bind9_node_snippet
  ]
}

resource "proxmox_virtual_environment_file" "master_loadbalancer_node_snippet" {
  content_type = "snippets"
  datastore_id = var.master_loadbalancer_node_snippet.datastore_id
  node_name    = var.master_loadbalancer_node_snippet.node_name

  source_file {
    path = local_file.master_loadbalancer_node_snippet.filename
  }

  depends_on = [
    proxmox_virtual_environment_file.ubuntu_2204_cloud_image,
    local_file.master_loadbalancer_node_snippet
  ]
}

resource "proxmox_virtual_environment_file" "master_control_plane_node_snippet" {
  content_type = "snippets"
  datastore_id = var.master_control_plane_node_snippet.datastore_id
  node_name    = var.master_control_plane_node_snippet.node_name

  source_file {
    path = local_file.master_control_plane_node_snippet.filename
  }

  depends_on = [
    proxmox_virtual_environment_file.ubuntu_2204_cloud_image,
    local_file.master_control_plane_node_snippet
  ]
}

resource "proxmox_virtual_environment_file" "additional_control_plane_node_snippet" {
  content_type = "snippets"
  datastore_id = var.additional_control_plane_node_snippet.datastore_id
  node_name    = var.additional_control_plane_node_snippet.node_name

  source_file {
    path = local_file.additional_control_plane_node_snippet.filename
  }

  depends_on = [
    proxmox_virtual_environment_file.ubuntu_2204_cloud_image,
    local_file.additional_control_plane_node_snippet
  ]
}

resource "proxmox_virtual_environment_file" "worker_node_snippet" {
  content_type = "snippets"
  datastore_id = var.worker_node_snippet.datastore_id
  node_name    = var.worker_node_snippet.node_name

  source_file {
    path = local_file.worker_node_snippet.filename
  }

  depends_on = [
    proxmox_virtual_environment_file.ubuntu_2204_cloud_image,
    local_file.worker_node_snippet
  ]
}