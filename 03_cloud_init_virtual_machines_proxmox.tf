resource "proxmox_virtual_environment_vm" "master_bind9_node" {
  for_each    = var.master_bind9_node
  name        = each.value.name
  description = "(Critical) Bind9 DNS server for internal use in k8s and LAN networks with autoupdating records via rndc.key."
  tags        = each.value.tags
  node_name   = each.value.node_name

  cpu {
    cores = each.value.cpu
  }

  memory {
    dedicated = each.value.memory
  }

  agent {
    enabled = true
  }

  network_device {
    bridge = each.value.network.bridge
  }

  disk {
    datastore_id = each.value.datastore_id
    file_id      = proxmox_virtual_environment_file.ubuntu_2204_cloud_image.id
    interface    = "scsi0"
    size         = each.value.disk
  }

  serial_device {} # The Debian cloud image expects a serial port to be present

  operating_system {
    type = "l26" # Linux Kernel 2.6 - 5.X.
  }

  initialization {
    user_data_file_id = proxmox_virtual_environment_file.master_bind9_node_snippet.id
    datastore_id      = each.value.datastore_id

    dns {
      server = each.value.network.dns
    }

    ip_config {
      ipv4 {
        address = each.value.network.ipv4_full
        gateway = each.value.network.gateway
      }
    }
  }

  depends_on = [
    proxmox_virtual_environment_file.master_bind9_node_snippet,
    proxmox_virtual_environment_file.ubuntu_2204_cloud_image
  ]
}


resource "proxmox_virtual_environment_vm" "master_loadbalancer_node" {
  for_each    = var.master_loadbalancer_node
  name        = each.value.name
  description = "(Critical) HAProxy loadbalancer for using as k8s endpoint for one, two or three control_plane nodes."
  tags        = each.value.tags
  node_name   = each.value.node_name

  cpu {
    cores = each.value.cpu
  }

  memory {
    dedicated = each.value.memory
  }

  agent {
    enabled = true
  }

  network_device {
    bridge = each.value.network.bridge
  }

  disk {
    datastore_id = each.value.datastore_id
    file_id      = proxmox_virtual_environment_file.ubuntu_2204_cloud_image.id
    interface    = "scsi0"
    size         = each.value.disk
  }

  serial_device {} # The Debian cloud image expects a serial port to be present

  operating_system {
    type = "l26" # Linux Kernel 2.6 - 5.X.
  }

  initialization {
    user_data_file_id = proxmox_virtual_environment_file.master_loadbalancer_node_snippet.id
    datastore_id      = each.value.datastore_id

    dns {
      server = each.value.network.dns
    }

    ip_config {
      ipv4 {
        address = each.value.network.ipv4_full
        gateway = each.value.network.gateway
      }
    }
  }

  depends_on = [
    proxmox_virtual_environment_file.master_loadbalancer_node_snippet,
    proxmox_virtual_environment_file.ubuntu_2204_cloud_image,
    proxmox_virtual_environment_vm.master_bind9_node
  ]
}


resource "proxmox_virtual_environment_vm" "master_control_plane_node" {
  for_each    = var.master_control_plane_node
  name        = each.value.name
  description = "(Critical) Master control_plane node of k8s cluster."
  tags        = each.value.tags
  node_name   = each.value.node_name

  cpu {
    cores = each.value.cpu
  }

  memory {
    dedicated = each.value.memory
  }

  agent {
    enabled = true
  }

  network_device {
    bridge = each.value.network.bridge
  }

  disk {
    datastore_id = each.value.datastore_id
    file_id      = proxmox_virtual_environment_file.ubuntu_2204_cloud_image.id
    interface    = "scsi0"
    size         = each.value.disk
  }

  serial_device {} # The Debian cloud image expects a serial port to be present

  operating_system {
    type = "l26" # Linux Kernel 2.6 - 5.X.
  }

  initialization {
    user_data_file_id = proxmox_virtual_environment_file.master_control_plane_node_snippet.id
    datastore_id      = each.value.datastore_id

    dns {
      server = each.value.network.dns
    }

    ip_config {
      ipv4 {
        address = each.value.network.ipv4_full
        gateway = each.value.network.gateway
      }
    }
  }

  depends_on = [
    proxmox_virtual_environment_file.master_control_plane_node_snippet,
    proxmox_virtual_environment_file.ubuntu_2204_cloud_image,
    proxmox_virtual_environment_vm.master_bind9_node,
    proxmox_virtual_environment_vm.master_loadbalancer_node
  ]
}


resource "proxmox_virtual_environment_vm" "additional_control_plane_nodes" {
  for_each    = var.additional_control_plane_nodes
  name        = each.value.name
  description = "Additional control_plane node of k8s cluster."
  tags        = each.value.tags
  node_name   = each.value.node_name

  cpu {
    cores = each.value.cpu
  }

  memory {
    dedicated = each.value.memory
  }

  agent {
    enabled = true
  }

  network_device {
    bridge = each.value.network.bridge
  }

  disk {
    datastore_id = each.value.datastore_id
    file_id      = proxmox_virtual_environment_file.ubuntu_2204_cloud_image.id
    interface    = "scsi0"
    size         = each.value.disk
  }

  serial_device {} # The Debian cloud image expects a serial port to be present

  operating_system {
    type = "l26" # Linux Kernel 2.6 - 5.X.
  }

  initialization {
    user_data_file_id = proxmox_virtual_environment_file.additional_control_plane_node_snippet.id
    datastore_id      = each.value.datastore_id

    dns {
      server = each.value.network.dns
    }

    ip_config {
      ipv4 {
        address = each.value.network.ipv4_full
        gateway = each.value.network.gateway
      }
    }
  }

  depends_on = [
    proxmox_virtual_environment_file.additional_control_plane_node_snippet,
    proxmox_virtual_environment_file.ubuntu_2204_cloud_image,
    proxmox_virtual_environment_vm.master_bind9_node,
    proxmox_virtual_environment_vm.master_loadbalancer_node,
    proxmox_virtual_environment_vm.master_control_plane_node
  ]
}


resource "proxmox_virtual_environment_vm" "master_worker_node" {
  for_each    = var.master_worker_node
  name        = each.value.name
  description = "(Critical) Master worker_node of k8s cluster."
  tags        = each.value.tags
  node_name   = each.value.node_name

  cpu {
    cores = each.value.cpu
  }

  memory {
    dedicated = each.value.memory
  }

  agent {
    enabled = true
  }

  network_device {
    bridge = each.value.network.bridge
  }

  disk {
    datastore_id = each.value.datastore_id
    file_id      = proxmox_virtual_environment_file.ubuntu_2204_cloud_image.id
    interface    = "scsi0"
    size         = each.value.disk
  }

  disk {
    datastore_id = each.value.datastore_id
    interface    = "scsi1"
    file_format  = "raw"
    size         = each.value.disk2 == 0 ? 1 : each.value.disk2
  }

  serial_device {} # The Debian cloud image expects a serial port to be present

  operating_system {
    type = "l26" # Linux Kernel 2.6 - 5.X.
  }

  initialization {
    user_data_file_id = proxmox_virtual_environment_file.worker_node_snippet.id
    datastore_id      = each.value.datastore_id

    dns {
      server = each.value.network.dns
    }

    ip_config {
      ipv4 {
        address = each.value.network.ipv4
        gateway = each.value.network.gateway
      }
    }
  }

  depends_on = [
    proxmox_virtual_environment_file.worker_node_snippet,
    proxmox_virtual_environment_file.ubuntu_2204_cloud_image,
    proxmox_virtual_environment_vm.master_bind9_node,
    proxmox_virtual_environment_vm.master_loadbalancer_node,
    proxmox_virtual_environment_vm.master_control_plane_node
  ]

}


resource "proxmox_virtual_environment_vm" "additional_worker_nodes" {
  for_each    = var.additional_worker_nodes
  name        = each.value.name
  description = "Additional worker_node of k8s cluster."
  tags        = each.value.tags
  node_name   = each.value.node_name

  cpu {
    cores = each.value.cpu
  }

  memory {
    dedicated = each.value.memory
  }

  agent {
    enabled = true
  }

  network_device {
    bridge = each.value.network.bridge
  }

  disk {
    datastore_id = each.value.datastore_id
    file_id      = proxmox_virtual_environment_file.ubuntu_2204_cloud_image.id
    interface    = "scsi0"
    size         = each.value.disk
  }

  disk {
    datastore_id = each.value.datastore_id
    interface    = "scsi1"
    file_format  = "raw"
    size         = each.value.disk2 == 0 ? 1 : each.value.disk2
  }

  serial_device {} # The Debian cloud image expects a serial port to be present

  operating_system {
    type = "l26" # Linux Kernel 2.6 - 5.X.
  }

  initialization {
    user_data_file_id = proxmox_virtual_environment_file.worker_node_snippet.id
    datastore_id      = each.value.datastore_id

    dns {
      server = each.value.network.dns
    }

    ip_config {
      ipv4 {
        address = each.value.network.ipv4_full
        gateway = each.value.network.gateway
      }
    }
  }

  depends_on = [
    proxmox_virtual_environment_file.worker_node_snippet,
    proxmox_virtual_environment_file.ubuntu_2204_cloud_image,
    proxmox_virtual_environment_vm.master_bind9_node,
    proxmox_virtual_environment_vm.master_loadbalancer_node,
    proxmox_virtual_environment_vm.master_control_plane_node,
    proxmox_virtual_environment_vm.master_worker_node
  ]
}
